# Tutoriel CSS

## Auteurs

* Florian Bergeret
* Mourad Bounajra
* Abdellah Khaldi
* Iablioune Youssef
-------------------
# Blending mode
## Définition et rappel W3C
Le blending mode permet de décrire une fusion entre différents éléments graphique grâce à différentes propriétés qui vont définir cette fusion.
Ces différentes propriétés sont : 

* normal
* multiply
* screen
* overlay
* darken
* lighten
* color-dodge
* color-burn
* hard-light
* soft-light
* difference
* exclusion
* hue
* saturation
* color
* luminosity

Comme il y a beaucoup de propriétés possible, il est difficile de se rappeler de leur but, c'est pourquoi on peut les regrouper en différentes catégories :

* assombrissement : darken, multiply et color-burn
* éclaircissement : lighten, screen et color-dodge
* contraste : overlay, soft-light et hard-light
* comparatif : difference et exclusion
* composante : hue, saturation, color et luminosity

Pour en savoir plus : [W3C](http://www.w3.org/TR/compositing-1/#mix-blend-mode)
## Tutoriel et utilisation du module
Pour utiliser ce module css il suffit de mettre :
```
selecteur{
	mix-blend-mode: propriete;
}
```
## Démonstration 
Voir src/index.html
## Implantation dans les navigateurs

* Internet explorer : Non supporté
* Edge : Non supporté
* Firefox : Version 32 et plus
* Chrome/Chrome mobile : Version 41 et plus
* Opera : Version 29 et plus
* Safari/Safari Mobile : Supporté partielement (ne supporte pas les options : hue, saturation et luminosity)

## Ressources
Différents site ont été utilisé pour la réalisation de ce tutoriel : 

* [mozilla developer](https://developer.mozilla.org/fr/docs/Web/CSS/blend-mode)
* [la-cascade](https://la-cascade.io/css-blend-modes/)
* [webdesignerdepot](http://www.webdesignerdepot.com/2014/07/15-css-blend-modes-that-will-supercharge-your-images/)

Ces trois sites ont été utilisés pour comprendre le fonctionnement de chacune des propriétés.